# README #

A simple bash script for converting a .PNG image to hex code block.


This repository also includes a pngquant binary which the script uses to optimise the PNG image(s) before converting to hex.

Website: https://pngquant.org

Source: https://github.com/pornel/pngquant

### Usage ###


```
#!shell

./GenerateEmbeddedCodeBlock.sh /PATH/TO/DIRECTORY_OF_PNG_FILES
```