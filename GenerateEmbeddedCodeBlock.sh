#!/bin/bash

# A script to optimise a .PNG file and convert it to a hex code block.
#
# The script uses the pngquant command line utility to convert 24/32-bit PNG
# images to paletted (8-bit) PNGs.
#
# Website:More info http://pngquant.org/
# Source:https://github.com/pornel/pngquant 
#
# Blackosx
#
# v0.3

SELF_PATH="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TMPDIR=/tmp/themeConvert
RESOURCESDIR="$SELF_PATH/Resources"
pngquant="$RESOURCESDIR/pngquant"

if [ "$1" == "" ]; then
    echo "No path supplied. Exiting"
    exit 1
elif [ ! -d "$1" ]; then
    echo "Error. Path does not exist. Exiting"
    exit 1
fi

mkdir -p "$1"/1_original "$1"/2_optimised "$1"/3_code_blocks

# Find all PNG files in the passed directory.
pngFiles=( $( find "$1" -type f -name "*.png" 2>/dev/null ) )
for (( p=0; p<${#pngFiles[@]}; p++ ))
do
  echo "Processing ${pngFiles[$p]}"
  
  # Make a copy of the original file
  cp "${pngFiles[$p]}" "$1"/1_original

  # Optimise PNG file
  "$pngquant" --force --ext .png -- "${pngFiles[$p]}"
  
  #Create Code Block
  outFile="$1"/3_code_Blocks/"${pngFiles[$p]##*/}"
  hexdump -v -e '"""0x" 1/1 "%02x, " ""' "${pngFiles[$p]}" > "$outFile".txt
  
  if [ -f "$outFile".txt ]; then

    fileId="${pngFiles[$p]##*/}"
    fileId="${fileId%.*}"
    
    # Write first line of file
    echo "UINT8 ${fileId}[] = {" >> "$outFile"_codeBlock.txt
    
    # Split hex file to desired line length
    grep -oE '.{1,96}' "$outFile".txt | sed 's/^/  /' >> "$outFile"_codeBlock.txt
    
    # Write closing line of file
    echo "};" >> "$outFile"_codeBlock.txt
    
    # Delete hex file
    rm "$outFile".txt
  fi

  # Move optimised PNG file to optimised folder
  mv "${pngFiles[$p]}" "$1"/2_optimised
 
done
echo "--------"  
echo "Complete"